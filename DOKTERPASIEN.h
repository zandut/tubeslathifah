#include <stdio.h>
#include <stdlib.h>
#include "boolean.h"
#ifndef DOKTERPASIEN_H_INCLUDED
#define DOKTERPASIEN_H_INCLUDED

typedef struct tElmtDok *adrDok;
typedef struct tElmtJad *adrJad;

//data dokter dan jadwalnya
typedef struct tElmtDok{
    char* nama;
    int kodeDok;
    char* dokter;
    adrJad firstJad;
    adrDok nextDok;
} ElmtDok;

typedef struct tElmtJad{
    char* hari;
    int pukul;
    adrJad nextJad;
}ElmtJad;

typedef struct{
    adrDok firstDok;
}ListDok;

typedef struct tElmtPasien *adrPasien;
typedef struct tElmtPasien {
	char nama; // Kewargakursian penonton tersebut
	int usia; // Filem yang diikuti penonton tersebut
	char keluhan;
	adrPasien next;
} ElmtPasien;

typedef struct {
	adrPasien first;
} ListPasien;

void createListDok(ListDok *LP);
adrDok alokasiDok(char* namaX, int kodeDokX,char* dokterX);
void dealokasiDok(adrDok p);
void insertFirstDok(ListDok *LP,adrDok p);
void insertLastDok(ListDok *LP,adrDok p);

adrJad alokasiJad(char* hariX,int pukulX);
void dealokasiJad(adrJad p);
void addDok(ListDok *LP, adrJad a,char nama[50]) ;
void addJad(ListDok *LP, char* nama, char* jadwal);

adrDok addressDok(ListDok LP, char nama[50]);

adrJad addressJad(ListDok LP, char nama[50], char hari[50]);

int JumlahPraktik(ListDok LP);
int cekJadwal(ListDok LP, char nama[50]);
void cetakdokter(ListDok LP);
void cetakjadwal(ListDok LP, char nama[50], int wow);

adrPasien alokasi(char* namaX, int usiaX, char* keluhanX);
void dealokasiPasien(adrPasien p);

int isPasienEmpty(ListPasien L);
void addPasien(ListPasien *L, char* nama[50], int usiaX, char* keluhan[50]);
#endif
